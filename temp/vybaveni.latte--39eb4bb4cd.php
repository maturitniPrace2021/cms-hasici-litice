<?php

use Latte\Runtime as LR;

/** source: template/vybaveni.latte */
final class Template39eb4bb4cd extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="cs">
  <head>
    <title>SDH LITICE</title>
    <meta charset="utf-8">
    <meta name=\'description\' content=\'Webová sránka Sboru dobrovolných hasičů Plzeň - Litice\'>
    <meta name=\'keywords\' content=\'hasiči, fireman, Litice, Plzeň - Litice, SDH Litice\'>
    <meta name=\'author\' content=\'Daniel Klečanský, Josef Löffler\'>
    <meta name=\'robots\' content=\'all\'>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="css/bootstrap10.min.css">  
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/styl.css">
    <link href=\'./images/icon.png\' rel=\'shortcut icon\' type=\'image/png\'>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

  <div class="site-wrap"  id="home-section">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>


    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

<div class="container">
  <div class="row align-items-center position-relative">



        <nav class="site-navigation text-center ml-auto" role="navigation">

          <a href="index.php" class="text-black site-logo"><span class="text-primary"><i class="fas fa-fire"></i> SDH LITICE</a>

          <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                  


                  <li><a href="index.php" class="nav-link"><i class="fas fa-home"></i> Domů</a></li>
                  <li><a href="kontakty.php" class="nav-link"><i class="fas fa-address-card"></i> Kontakty</a></li>
                  <li><a href="fotogalerie.php" class="nav-link"><i class="fas fa-images"></i> Fotogalerie</a></li>
                  <li><a href="udalosti.php" class="nav-link"><i class="far fa-calendar-alt"></i> Události</a></li>
                  <li><a class="active" href="vybaveni.php" class="nav-link"><i class="fas fa-truck-moving"></i> Vybavení</a></li>
                  <li><a href="diskuze.php" class="nav-link"><i class="fas fa-users"></i> Diskuze</a></li>
                  <li><a><button class="butto butto-1" onclick="window.location.href=\'registrace.php\'">Přihlášení</button></a></li>
          </ul>
        </nav>


          <div class="toggle-button d-inline-block d-lg-none"><a href="#" class="site-menu-toggle py-5 js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          <a class="gototopbtn" href="#"><i class="fas fa-arrow-up"></i></a>

        </div>
      </div>

    </header>

      <div class="site-section" id="technika-section">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center mb-5">
            <div class="block-heading-1">
              <h2>Vybavení</h2>
              </div>
          </div>
  


          <div class="vybaveni">
';
		$x = 0 /* line 81 */;
		$iterations = 0;
		foreach ($article as $item) /* line 82 */ {
			echo '          <div class="car">
        <h2><a href="">';
			echo LR\Filters::escapeHtmlText($article[$x]) /* line 84 */;
			echo '</a></h2>
        <p>';
			echo LR\Filters::escapeHtmlText($content[$x]) /* line 85 */;
			echo '</p>
        <a><img src="';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($img[$x])) /* line 86 */;
			echo '" class="img-fluid rounded" width="500px" height="380px"></a>
        <p><a href="';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($odkaz[$x])) /* line 87 */;
			echo '">více informací</a><p> <br><br>
        </div>
';
			$x = $x + 1 /* line 89 */;
			echo '        

      

';
			$iterations++;
		}
		echo '
            </div>
            </div>
          </div>
        </div>
        
        



    <div class="pt-5">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <div class="block-heading-1">
              <h2>Jedni ze sponzorů</h2>
            </div>
          </div>
        </div>
        <div class="row align-items-center sponzor">
          <div class="col-md-3">
            <img src="images/sponzor2.png" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-3">
            <img src="images/sponzor3.jpg" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-3">
            <img src="images/sponzor4.png" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-3">
            <img src="images/sponzor5.jpg" alt="Image" class="img-fluid">
          </div>
        </div>
      </div>
    </div>

    <a class="gototopbtn" href="#"><i class="fas fa-arrow-up"></i></a>

    <footer class="site-footer">
      <div class="container">
        
              <div class="col-md-4 ml-auto"> 
                <h2 class="footer-heading mb-4">©2021 Daniel Klečanský & Josef Löffler</h2>
                <ul class="list-unstyled">
                  <li><a href="index.php">Domů</a></li>
                  <li><a href="kontakty.php">Kontakty</a></li>
                  <li><a href="fotogalerie.php">Fotogalerie</a></li>
                  <li><a href="udalosti.php">Události</a></li>
                  <li><a href="vybaveni.php">Vybavení</a></li>
                  <li><a href="diskuze.php">Diskuze</a></li>
                  <li><a href="registrace.php">Přihlášení</a></li>

                </ul>
             
          </div>
        </div>
        
    </footer>

  </div>

  <script src="js/jquery-3.3.1.min.js"></script>


  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/jquery.sticky.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/main.js"></script>

  </body>
</html>
';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['item' => '82'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
