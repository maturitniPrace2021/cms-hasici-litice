<?php

use Latte\Runtime as LR;

/** source: template/Index.latte */
final class Templated97d866f02 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="cs">
  <head>
    <title>SDH LITICE</title>
    <meta charset="utf-8">
    <meta name=\'description\' content=\'Webová sránka Sboru dobrovolných hasičů Plzeň - Litice\'>
    <meta name=\'keywords\' content=\'hasiči, fireman, Litice, Plzeň - Litice, SDH Litice\'>
    <meta name=\'author\' content=\'Daniel Klečanský, Josef Löffler\'>
    <meta name=\'robots\' content=\'all\'>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="css/bootstrap1.min.css">  
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/styl.css">
    <link href=\'images/icon.png\' rel=\'shortcut icon\' type=\'image/png\'>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

  <div class="site-wrap"  id="home-section">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>


    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

<div class="container">
  <div class="row align-items-center position-relative">



        <nav class="site-navigation text-center ml-auto" role="navigation">

          <a href="index.php" class="text-black site-logo"><span class="text-primary"><i class="fas fa-fire"></i> SDH LITICE</a>

          <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                  


                  <li><a class="active" href="index.php" class="nav-link"><i class="fas fa-home"></i> Domů</a></li>
                  <li><a href="kontakty.php" class="nav-link"><i class="fas fa-address-card"></i> Kontakty</a></li>
                  <li><a href="fotogalerie.php" class="nav-link"><i class="fas fa-images"></i> Fotogalerie</a></li>
                  <li><a href="udalosti.php" class="nav-link"><i class="far fa-calendar-alt"></i> Události</a></li>
                  <li><a href="vybaveni.php" class="nav-link"><i class="fas fa-truck-moving"></i> Vybavení</a></li>
                  <li><a href="diskuze.php" class="nav-link"><i class="fas fa-users"></i> Diskuze</a></li>
                  <li><a><button class="butto butto-1" onclick="window.location.href=\'registrace.php\'">Přihlášení</button></a></li>
                </ul>
              </nav>

              

          <div class="toggle-button d-inline-block d-lg-none"><a href="#" class="site-menu-toggle py-5 js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          <a class="gototopbtn" href="#"><i class="fas fa-arrow-up"></i></a>

        </div>
      </div>

    </header>


    <div class="owl-carousel slide-one-item">


      <div class="site-section-cover overlay img-bg-section" style="background-image: url(\'images/background1.jpg\'); " >
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-12 col-lg-7 text-center">
              <h1 style="position: fixed; margin-top: 350px;">Sbor dobrovolných hasičů Plzeň - Litice</h1>
              <p></p>
            </div>
          </div>
        </div>

      </div>

      <div class="site-section-cover overlay img-bg-section" style="background-image: url(\'images/has1.jpg\'); " >
        <div class="container sdh">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-12 col-lg-7 text-center" >
              <h1 style="position: fixed; margin-top: 350px;">Sbor dobrovolných hasičů Plzeň - Litice</h1>
              <p></p>
            </div>
          </div>
        </div>

      </div>


    </div>


    <div class="pb-5" style="position: relative; z-index: 8;">
      <div class="container">
      <div class="uvodky">
        <div class="row" style="margin-top: -50px; display: flex;">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
            <img src="images/has26.jpg" alt="Image" class="img-fluid mb-3">
            <h3 class="text-primary h4 mb-2"></h3>
            <p></p>
            <p><a href="#"></a></p>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
            <img src="images/has11.jpg" alt="Image" class="img-fluid mb-3">
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
            <img src="images/has5.jpg" alt="Image" class="img-fluid mb-3">
            <h3 class="text-primary h4 mb-2"></h3>
            <p></p>
            <p><a href="#"></a></p>
          </div>
        </div>
      </div>
    </div>


    <div class="site-section" id="investors-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-lg-6 mb-4">
                <img src="images/has13.jpg" alt="Image" class="img-fluid">
              </a>
            
          </div>
          <div class="col-lg-5 ml-auto">
            <h2 class="text-primary mb-4">Něco o nás</h2>
            <p>IČO: 66360145</p>
            <p><i class="fas fa-map-marker-alt"></i> Adresa: Klatovská 25/211, Plzeň 6 – Litice, 321 00</p>
            <p><i class="fas fa-money-check-alt"></i> Bankovní účet: 213174565 / 0300 (ČSOB)</p>
            <p><i class="fas fa-at"></i> E-mail: sdh.litice@gmail.com</p>
            <p><i class="fas fa-phone"></i> Tel.: +420 725 450 989</p>
            <p>Mimo činnost jednotky se zabýváme také mládeží.</p>
          </div>
        </div>
      </div>
    </div>
    <br><br><br><br>

    
    <div class="pt-5">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <div class="block-heading-1">
              <h2>Jedni ze sponzorů</h2>
            </div>
          </div>
        </div>
        <div class="row align-items-center sponzor">
          <div class="col-md-3">
            <img src="images/sponzor2.png" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-3">
            <img src="images/sponzor3.jpg" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-3">
            <img src="images/sponzor4.png" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-3">
            <img src="images/sponzor5.jpg" alt="Image" class="img-fluid">
          </div>
        </div>
      </div>
    </div>




    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="row">

              <div class="col-md-4 ml-auto">
                <h2 class="footer-heading mb-4">©2021</h2>
                <ul class="list-unstyled">
                  <li><a href="index.php">Domů</a></li>
                  <li><a href="kontakty.php">Kontakty</a></li>
                  <li><a href="fotogalerie.php">Fotogalerie</a></li>
                  <li><a href="udalosti.php">Události</a></li>
                  <li><a href="vybaveni.php">Vybavení</a></li>
                  <li><a href="diskuze.php">Diskuze</a></li>
                  <li><a href="registrace.php">Přihlášení</a></li>

                </ul>
              </div>

            </div>
          </div>
          <div class="col-md-4 ml-auto">

            <div class="mb-5">
              <div class="mb-5">
                <p>Stránku se budeme snažit pravidelně udržovat a obnovovat.</p>
              </div>
              </div>
          </div>
        </div>
        </div>
      </div>
    </footer>

  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  
  
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/jquery.sticky.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/main.js"></script>

  </body>
</html>

';
		return get_defined_vars();
	}

}
