<?php


class detailArticle
{
private $id;
private $popis;
private $img;



  function __construct()
  {
  }
  public function getId(){
      return $this->id;
  }
  public function getpopis(){
    return $this->popis;
}

  public function getimg(){
      return $this->img;
  }
 



public function setpopis($popis){

    $this->popis = $popis;

}


  public function setimg($img){

          $this->img = $img;

  }


  public function loadDataById($id){
      $feedBack = true; // počáteční nastavení návratové hodnoty
      $dbConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME); // vytvoření objektu mysqli - paramerty převzaty z config.php

      if ($dbConn->connect_error) {
          $feedBack = false; // nastavení návratové hodnoty na false v případě chyby s připojením k DB serveru
      }

      $sql = "SET CHARACTER SET UTF8"; // SQL dotaz nastavující kódovou stránku pro komunikaci s DB serverem
      $dbConn->query($sql); // odeslání SQL dotazu na DB server

      $sql = "SELECT * FROM fotogalerie WHERE id='$id'"; // SQL dotaz pro výběr článku dle id z tabulky articles

      $result = $dbConn->query($sql); // odeslání SQL dotazu na DB server - $result obsahuje výsledek dotazu

      if ($result->num_rows > 0) { // kontrola zdali SQL dotaz SELECT vrátil článek
          while($row = $result->fetch_assoc()) { // postupné procházení řádek výsledku - fetch_assoc() vrací pole hodnot jednoho řádku
              $this->id = $row["id"]; // přiřazení hodnoty id z pole $row do atributu objektu
              $this->popis = $row["popis"];
              $this->img = $row["img"];
         
          }
      }
          else {
              $feedBack = false; // v případě, že DB server nevrátí žádný záznam - výstupní hodnota False
          }
      $dbConn->close();
      return $feedBack;
  }







}



class listOfArticles{
    private $listArticles;

    public function __construct(){

    }

    public function getListArticles(){
        return $this->listArticles;
    }


    /* načtení seznamu článků dle zadané kategorie */
    public function loadListArticlesByCategory($category){
        $sql= "SELECT * FROM fotogalerie ";
        $feedBack = true; // počáteční nastavení návratové hodnoty
        $dbConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME); // vytvoření objektu mysqli - paramerty převzaty z config.php

        if ($dbConn->connect_error) {
            $feedBack = false; // nastavení návratové hodnoty na false v případě chyby s připojením k DB serveru
        }

        $sqlCharSet = "SET CHARACTER SET UTF8"; // SQL dotaz nastavující kódovou stránku pro komunikaci s DB serverem
        $dbConn->query($sqlCharSet); // odeslání SQL dotazu na DB server

        $result = $dbConn->query($sql);
        if ($result->num_rows > 0) { // kontrola zdali SQL dotaz SELECT vrátil články
            while($row = $result->fetch_assoc()) { // postupné procházení řádek výsledku - fetch_assoc() vrací pole hodnot jednoho řádku
                $actArticle = new detailArticle(); // vytvoření nového objektu článku

               
                $actArticle->setpopis($row["popis"]);
                $actArticle->setimg($row["img"]);
               
                 //naplnění atributů objektu hodnotami
                $this->listArticles[] = $actArticle; // přidání objektu do pole článků
            }
        }
            else {
                $feedBack = false; // v případě, že DB server nevrátí žádný záznam - výstupní hodnota False
            }
        $dbConn->close();
        return $feedBack;
    }






}






















 ?>