
<?php


class detailArticle
{
private $id;
private $nameArticle;
private $contentArticle;
private $img;
private $odkaz;


  function __construct()
  {
  }
  public function getId(){
      return $this->id;
  }

  public function getNameArticle(){
      return $this->nameArticle;
  }
  public function getContentArticle(){
      return $this->contentArticle;
  }

  public function getimg(){
      return $this->img;
  }
  public function getodkaz(){
      return $this->odkaz;
  }





  public function setNameArticle($nameArticle){

          $this->nameArticle = $nameArticle;

  }
  public function setcontentArticle($contentArticle){

          $this->contentArticle = $contentArticle;

  }

  public function setimg($img){

          $this->img = $img;

  }
  public function setodkaz($odkaz){

          $this->odkaz = $odkaz;

  }

  public function loadDataById($id){
      $feedBack = true; // počáteční nastavení návratové hodnoty
      $dbConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME); // vytvoření objektu mysqli - paramerty převzaty z config.php

      if ($dbConn->connect_error) {
          $feedBack = false; // nastavení návratové hodnoty na false v případě chyby s připojením k DB serveru
      }

      $sql = "SET CHARACTER SET UTF8"; // SQL dotaz nastavující kódovou stránku pro komunikaci s DB serverem
      $dbConn->query($sql); // odeslání SQL dotazu na DB server

      $sql = "SELECT * FROM articles WHERE id='$id'"; // SQL dotaz pro výběr článku dle id z tabulky articles

      $result = $dbConn->query($sql); // odeslání SQL dotazu na DB server - $result obsahuje výsledek dotazu

      if ($result->num_rows > 0) { // kontrola zdali SQL dotaz SELECT vrátil článek
          while($row = $result->fetch_assoc()) { // postupné procházení řádek výsledku - fetch_assoc() vrací pole hodnot jednoho řádku
              $this->id = $row["id"]; // přiřazení hodnoty id z pole $row do atributu objektu

              $this->nameArticle = $row["nameArticles"];
              $this->contentArticle = $row["contentArticles"];

              $this->img = $row["img"];
              $this->odkaz = $row["odkaz"];
          }
      }
          else {
              $feedBack = false; // v případě, že DB server nevrátí žádný záznam - výstupní hodnota False
          }
      $dbConn->close();
      return $feedBack;
  }







}



class listOfArticles {
    private $listArticles;

    public function __construct(){

    }

    public function getListArticles(){
        return $this->listArticles;
    }


    /* načtení seznamu článků dle zadané kategorie */
    public function loadListArticlesByCategory($category){
        $sql= "SELECT * FROM articles ";
        $feedBack = true; // počáteční nastavení návratové hodnoty
        $dbConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME); // vytvoření objektu mysqli - paramerty převzaty z config.php

        if ($dbConn->connect_error) {
            $feedBack = false; // nastavení návratové hodnoty na false v případě chyby s připojením k DB serveru
        }

        $sqlCharSet = "SET CHARACTER SET UTF8"; // SQL dotaz nastavující kódovou stránku pro komunikaci s DB serverem
        $dbConn->query($sqlCharSet); // odeslání SQL dotazu na DB server

        $result = $dbConn->query($sql);
        if ($result->num_rows > 0) { // kontrola zdali SQL dotaz SELECT vrátil články
            while($row = $result->fetch_assoc()) { // postupné procházení řádek výsledku - fetch_assoc() vrací pole hodnot jednoho řádku
                $actArticle = new detailArticle(); // vytvoření nového objektu článku

                $actArticle->setNameArticle($row["nameArticle"]);
                $actArticle->setcontentArticle($row["contentArticle"]);
                $actArticle->setimg($row["img"]);
                $actArticle->setodkaz($row["odkaz"]); //naplnění atributů objektu hodnotami
                $this->listArticles[] = $actArticle; // přidání objektu do pole článků
            }
        }
            else {
                $feedBack = false; // v případě, že DB server nevrátí žádný záznam - výstupní hodnota False
            }
        $dbConn->close();
        return $feedBack;
    }






}






















 ?>