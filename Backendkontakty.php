<?php


class detailArticle
{
private $id;
private $titul;
private $Firstname;
private $Lastname;
private $email;
private $phone;
private $Obrazek;
private $pozice;


  function __construct()
  {
  }
  public function getId(){
      return $this->id;
  }
  public function gettitul(){
    return $this->titul;
}

  public function getFirstname(){
      return $this->Firstname;
  }
  public function getLastname(){
    return $this->Lastname;
}
  public function getemail(){
      return $this->email;
  }

  public function getphone(){
      return $this->phone;
  }
  public function getObrazek(){
      return $this->Obrazek;
  }
  public function getpozice(){
    return $this->pozice;
}



public function settitul($titul){

    $this->titul = $titul;

}


  public function setFirstname($Firstname){

          $this->Firstname = $Firstname;

  }
  public function setLastname($Lastname){

    $this->Lastname = $Lastname;

}
  public function setemail($email){

          $this->email = $email;

  }

  public function setphone($phone){

          $this->phone = $phone;

  }
  public function setObrazek($Obrazek){

          $this->Obrazek = $Obrazek;

  }
  public function setpozice($pozice){

    $this->pozice = $pozice;

}

  public function loadDataById($id){
      $feedBack = true; // počáteční nastavení návratové hodnoty
      $dbConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME); // vytvoření objektu mysqli - paramerty převzaty z config.php

      if ($dbConn->connect_error) {
          $feedBack = false; // nastavení návratové hodnoty na false v případě chyby s připojením k DB serveru
      }

      $sql = "SET CHARACTER SET UTF8"; // SQL dotaz nastavující kódovou stránku pro komunikaci s DB serverem
      $dbConn->query($sql); // odeslání SQL dotazu na DB server

      $sql = "SELECT * FROM Kontakty WHERE id='$id'"; // SQL dotaz pro výběr článku dle id z tabulky articles

      $result = $dbConn->query($sql); // odeslání SQL dotazu na DB server - $result obsahuje výsledek dotazu

      if ($result->num_rows > 0) { // kontrola zdali SQL dotaz SELECT vrátil článek
          while($row = $result->fetch_assoc()) { // postupné procházení řádek výsledku - fetch_assoc() vrací pole hodnot jednoho řádku
              $this->id = $row["id"]; // přiřazení hodnoty id z pole $row do atributu objektu
              $this->titul = $row["titul"];
              $this->Firstname = $row["Firstname"];
              $this->Lastname = $row["Lastname"];
              $this->email = $row["email"];

              $this->phone = $row["phone"];
              $this->obrazek = $row["obrazek"];
              $this->pozice = $row["pozice"];
          }
      }
          else {
              $feedBack = false; // v případě, že DB server nevrátí žádný záznam - výstupní hodnota False
          }
      $dbConn->close();
      return $feedBack;
  }







}



class listOfArticles{
    private $listArticles;

    public function __construct(){

    }

    public function getListArticles(){
        return $this->listArticles;
    }


    /* načtení seznamu článků dle zadané kategorie */
    public function loadListArticlesByCategory($category){
        $sql= "SELECT * FROM kontakty ";
        $feedBack = true; // počáteční nastavení návratové hodnoty
        $dbConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME); // vytvoření objektu mysqli - paramerty převzaty z config.php

        if ($dbConn->connect_error) {
            $feedBack = false; // nastavení návratové hodnoty na false v případě chyby s připojením k DB serveru
        }

        $sqlCharSet = "SET CHARACTER SET UTF8"; // SQL dotaz nastavující kódovou stránku pro komunikaci s DB serverem
        $dbConn->query($sqlCharSet); // odeslání SQL dotazu na DB server

        $result = $dbConn->query($sql);
        if ($result->num_rows > 0) { // kontrola zdali SQL dotaz SELECT vrátil články
            while($row = $result->fetch_assoc()) { // postupné procházení řádek výsledku - fetch_assoc() vrací pole hodnot jednoho řádku
                $actArticle = new detailArticle(); // vytvoření nového objektu článku

                $actArticle->setObrazek($row["Obrazek"]);
                $actArticle->settitul($row["titul"]);
                $actArticle->setFirstname($row["Firstname"]);
                $actArticle->setLastname($row["Lastname"]);
                $actArticle->setpozice($row["pozice"]);
                $actArticle->setphone($row["phone"]);
                $actArticle->setemail($row["email"]); //naplnění atributů objektu hodnotami
                $this->listArticles[] = $actArticle; // přidání objektu do pole článků
            }
        }
            else {
                $feedBack = false; // v případě, že DB server nevrátí žádný záznam - výstupní hodnota False
            }
        $dbConn->close();
        return $feedBack;
    }






}






















 ?>