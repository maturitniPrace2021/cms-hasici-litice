-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 07, 2021 at 07:24 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hasici`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `ID` int(11) NOT NULL,
  `nameArticle` varchar(2000) CHARACTER SET utf16 COLLATE utf16_czech_ci NOT NULL,
  `contentArticle` longtext CHARACTER SET utf16 COLLATE utf16_czech_ci NOT NULL,
  `img` varchar(300) CHARACTER SET utf16 COLLATE utf16_czech_ci NOT NULL,
  `odkaz` varchar(500) CHARACTER SET utf16 COLLATE utf16_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`ID`, `nameArticle`, `contentArticle`, `img`, `odkaz`) VALUES
(5, 'SCANIA P500 6x6', 'TECHNICKÉ PARAMETRY=\r\n                -délka: 9700 mm \r\n                -šířka: 2550 mm \r\n                 -výška: 3200 mm \r\n                -hmotnost: 27 000 kg \r\n                 -výkon motoru: 368 kW \r\n                -maximální rychlost:\r\n              -maximální provozní tlak čerpadla: \r\n                -nádrž na vodu: 9000 litrů \r\n               -nádrž na pěnu: 500 litrů \r\n\r\n             ', 'images/has26.jpg\r\n', 'https://www.tht.cz/cs/cisternova-automobilova-strikacka/tezka/cas-30-9000-540-s-2-z-scania-p440-6x6'),
(6, 'DA M1Z RENAULT MIDLUM 4X2', 'TECHNICKÉ PARAMETRY=\r\n                   -délka: 6420 mm \r\n                     -šířka: 2370 mm \r\n                     -výška: 3050 mm \r\n                   -hmotnost: 10 000 kg \r\n                  -výkon motoru: 161 kW/2 300 min-1 \r\n                  -maximální rychlost: 90 km/h \r\n                 -maximální provozní tlak čerpadla: 1,6 MPa \r\n                  -karoserie: skříňová \r\n                   -počet míst k sezení: 2+4\r\n                    -pomocná zařízení: přenosová motorová stříkačka\r\n', 'images/has8.jpg', 'https://www.tht.cz/cs/zasahove-pozarni-automobily/dopravni-automobil/dopravni-automobil-renault-midlum-7043-60'),
(12, 'CAS 24 IVECO', 'TECHNICKÉ PARAMETRY=\r\n                -délka: 7250 mm \r\n                -šířka: 2500 mm \r\n                 -výška: 3150 mm \r\n                -hmotnost: 15 000 kg \r\n                 -výkon motoru: 196 kW \r\n                -maximální rychlost: 105 km/h\r\n              -maximální provozní tlak čerpadla: 1,6 MPa \r\n                -nádrž na vodu: 3000 litrů \r\n               -nádrž na pěnu: 300 litrů \r\n\r\n             ', 'images/has2-1.jpg\r\n', 'https://www.pozary.cz/clanek/85-cas-24-iveco-hzs-hl-m-prahy/');

-- --------------------------------------------------------

--
-- Table structure for table `diskuze`
--

CREATE TABLE `diskuze` (
  `ID` bigint(20) NOT NULL,
  `text` varchar(2500) NOT NULL,
  `datum` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `diskuze`
--

INSERT INTO `diskuze` (`ID`, `text`, `datum`) VALUES
(24, '', ''),
(25, 'ppp', '07.04.2021'),
(26, 'dd', '07.04.2021'),
(27, 'Pepa je borec', '07.04.2021');

-- --------------------------------------------------------

--
-- Table structure for table `fotogalerie`
--

CREATE TABLE `fotogalerie` (
  `ID` int(11) NOT NULL,
  `popis` varchar(50) NOT NULL,
  `img` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fotogalerie`
--

INSERT INTO `fotogalerie` (`ID`, `popis`, `img`) VALUES
(2, 'Auta', 'images/has1.jpg'),
(3, 'CAS 24 IVECO', 'images/has2-1.jpg'),
(4, 'Auta', 'images/has3.jpg'),
(5, 'Auta', 'images/has4.jpg'),
(6, 'Auta', 'images/has5.jpg'),
(7, 'Auta', 'images/has6.jpg'),
(8, 'DA M1Z RENAULT MIDLUM', 'images/has8.jpg'),
(9, 'Otevření hasičské zbrojnice', 'images/has9.jpg'),
(10, 'Den otevřených dveří', 'images/denod.jpg'),
(11, 'Den s IZS', 'images/has10.jpg'),
(12, 'Dětská soutěž', 'images/has11.jpg'),
(13, 'Jednotka', 'images/has12.jpg'),
(14, 'Požární zbrojnice', 'images/background3.jpg'),
(15, 'Detail zbrojnice', 'images/img_1539.jpg'),
(17, 'Desinfekce veřejných prostor', 'images/has14.jpg'),
(26, 'Desinfekce veřejných prostor', 'images/has15.jpg'),
(27, 'Desinfekce veřejných prostor', 'images/has16.jpg'),
(28, 'Drony SIT', 'images/has17.jpg'),
(29, 'Stavění vánočního stromu', 'images/has18.jpg'),
(30, 'Oprava zámkové dlažby', 'images/has19.jpg'),
(31, 'Výstavba nových automatických garážových vrat', 'images/has20.jpg'),
(32, 'Požár', 'images/has21.jpg'),
(36, 'Povodně - Borská pole', 'images/has22.jpg'),
(37, 'Povodně - Výsluní', 'images/has23.jpg'),
(38, 'Povodně - Výsluní', 'images/has24.jpg'),
(39, 'Návštěva nově stavěné cisterny', 'images/has25.jpg'),
(40, 'SCANIE P500 6x6', 'images/has26.jpg'),
(41, 'SCANIE P500 6x6', 'images/has27.jpg'),
(42, 'SCANIE P500 6x6', 'images/has28.jpg'),
(43, 'SCANIE P500 6x6', 'images/has29.jpg'),
(44, 'SCANIE P500 6x6', 'images/has30.jpg'),
(45, 'SCANIE P500 6x6', 'images/has31.jpg'),
(47, 'nevim', 'https://www.w3schools.com/tags/tag_option.asp'),
(48, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kontakty`
--

CREATE TABLE `kontakty` (
  `ID` int(11) NOT NULL,
  `titul` varchar(50) NOT NULL,
  `Firstname` varchar(50) NOT NULL,
  `Lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `Obrazek` varchar(100) NOT NULL,
  `pozice` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kontakty`
--

INSERT INTO `kontakty` (`ID`, `titul`, `Firstname`, `Lastname`, `email`, `phone`, `Obrazek`, `pozice`) VALUES
(1, 'Bc.', 'Václav', 'Král', '', '+420 724 660 552', 'images/prof1.png', 'Starosta'),
(2, '', 'Rudolf ', 'Klečanský', '', '+420 776 404 488', 'images/prof1.png', 'Velitel JPO III'),
(3, '', 'Petra', 'Polívková', '', '+420 724 930 096', 'images/prof2.png', 'Hospodářka'),
(4, '', 'Alena', 'Klečanská', 'klecanskaa@seznam.cz', '+420 604 668 176', 'images/prof2.png', 'Vedoucí kolektivu mládeže'),
(6, '', 'Michal', 'Hausner', '', '+420 776 181 180', 'images/prof1.png', 'Náměstek starosty sboru'),
(7, '', 'Markéta', 'Löfflerová', '', '+420 777 816 360', 'images/prof2.png', 'Jednatelka sboru'),
(8, 'Mgr.', 'Jaroslav', 'Balvín', '', '+420 728 643 309', 'images/prof1.png', 'Revizor sboru'),
(9, '', 'Dagmar', 'Jelinková', '', '+420 724 300 123', 'images/prof2.png', 'Člen výboru sboru'),
(10, '', 'Miroslav', 'Moule', '', '+420 725 045 340', 'images/prof1.png', 'Člen výboru sboru'),
(11, '', 'Lukáš', 'Fořt', '', '+420 603 778 393', 'images/prof1.png', 'Člen výboru sboru'),
(12, '', 'Lukáš', 'Mach', '', '+420 731 209 160', 'images/prof1.png', 'Člen výboru sboru'),
(13, '', 'Ladislav', 'Fišera', '', '+420 776 468 935', 'images/prof1.png', 'Člen výboru sboru');

-- --------------------------------------------------------

--
-- Table structure for table `udalosti`
--

CREATE TABLE `udalosti` (
  `ID` int(11) NOT NULL,
  `Nazev` varchar(300) CHARACTER SET utf32 COLLATE utf32_czech_ci NOT NULL,
  `Typ` set('novinky','soutěže','společenské akce') CHARACTER SET utf32 COLLATE utf32_czech_ci NOT NULL,
  `obsah` varchar(2500) CHARACTER SET ucs2 COLLATE ucs2_czech_ci NOT NULL,
  `img` varchar(1000) CHARACTER SET utf32 COLLATE utf32_czech_ci NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `udalosti`
--

INSERT INTO `udalosti` (`ID`, `Nazev`, `Typ`, `obsah`, `img`, `datum`) VALUES
(2, 'PLAMEN 2018/2019 ZRUŠEN!', 'soutěže', 'Vedení Městského sdružení Plzeň zrušilo soutěž Plamen, která se měla konat v termínu 25. - 26. 5. 2019.', 'images/img_2.jpg', '2019-05-25'),
(3, 'Den otevřených dveří pro veřejnost', 'novinky', 'V sobotu 18. 5. 2019 proběhl Den otevřených dveří v nové litické požární zbrojnici.', 'images/denod.jpg', '2019-05-18'),
(4, 'Výstavba nové hasičské zbrojnice dokončena!', 'novinky', 'Výstavba nové hasičské zbrojnice byla konečně dokončena.', 'https://www.plzen.cz/wp-content/uploads/2020/01/IMG_20200122_094902-1024x768.jpg', '2018-10-15'),
(5, 'Dětská soutěž v Plzni - Božkově', 'soutěže', 'O víkendu se pořádala další dětská soutěž, tentokrát v Plzni - Božkově, naše družstva se umístila na 1. a 2. místě.', 'images/has11.jpg', '2018-10-13'),
(6, 'Desinfekce veřejných prostor', 'novinky', 'Naše jednotka dnes desinfikovala veřejné prostory v Plzni - Liticích, Plzni - Lhotě a na Valše.', 'images/has14.jpg', '2021-03-04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `Jmeno` varchar(30) CHARACTER SET utf16 COLLATE utf16_czech_ci NOT NULL,
  `Heslo` varchar(30) CHARACTER SET utf16 COLLATE utf16_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `Jmeno`, `Heslo`) VALUES
(1, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `diskuze`
--
ALTER TABLE `diskuze`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `fotogalerie`
--
ALTER TABLE `fotogalerie`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `kontakty`
--
ALTER TABLE `kontakty`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `udalosti`
--
ALTER TABLE `udalosti`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `diskuze`
--
ALTER TABLE `diskuze`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `fotogalerie`
--
ALTER TABLE `fotogalerie`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `kontakty`
--
ALTER TABLE `kontakty`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `udalosti`
--
ALTER TABLE `udalosti`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
